protoc  --go_out=. --go-grpc_out=. --go-grpc_opt=require_unimplemented_servers=false protos/*.proto

protoc --micro_out=. --go_out=. protos/*.proto